<?php
/**
 * @file
 * feature_logintoboggan.features.user_role.inc
 */

/**
 * Implementation of hook_user_default_roles().
 */
function feature_logintoboggan_user_default_roles() {
  $roles = array();

  // Exported role: preauthorized
  $roles['preauthorized'] = array(
    'name' => 'preauthorized',
    'weight' => '4',
  );

  return $roles;
}
